WORKDIR=$1
LUSER="aluno"

support()
{
	APP_LIST=$@
	for app in APP_LIST; do
		if [ -a $WORKDIR/$ROOM/scripts/$app/support ]; then
			SUPPORT_LIST=$(cat $WORKDIR/$ROOM/scripts/$app/support)
			for sup_app in $SUPPORT_LIST; do
				sudo apt -y install $sup_app
			done
		fi
	done
}

atom()
{
	support atom
	wget https://atom.io/download/deb -O /tmp/atom.deb
	sudo dpkg --install /tmp/atom.deb
	rm /tmp/atom.deb
}


dpkgs()
{
	atom
	apt -y --fix-broken install
}

arduino()
{
	support arduino
	if [ -d $WORKDIR/$ROOM/scripts/arduino ]; then
		cp -r $WORKDIR/$ROOM/scripts/arduino/arduino /usr/share/
		sudo chmod 777 /usr/share/arduino/install.sh
		bash /usr/share/arduino/install.sh
		usermod -a -G dialout $LUSER #Arduino's door permission fix
	else
		echo "Sem binário do arduino"
	fi
}

netbeans()
{

	wget http://ftp.unicamp.br/pub/apache/netbeans/netbeans/11.3/Apache-NetBeans-11.3-bin-linux-x64.sh -O /tmp/apache.sh
	sudo chmod 777 /tmp/apache.sh
	bash /tmp/apache.sh --silent
}

aptitudes()
{
	if [ -a $WORKDIR/$ROOM/scripts/aptitude_list ]; then
		for i in $(cat $WORKDIR/$ROOM/scripts/aptitude_list); do
			aptitude -y install $i
		done
	fi
}

dpkgs
aptitudes
arduino
netbeans

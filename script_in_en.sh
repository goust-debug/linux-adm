#!/bin/bash

look_in()
{
#Search a file for a specified mac
	mac_list=($(cat $1))	#We read the macs, into a vector

	for i in ${mac_list[@]}; do
		if [ "$mac" = "$(echo $i | cut -b 4-20)" ]; then #Cut the mac, and compare
			echo "$(echo $i | cut -b 1-2)" #If found, return the pc number to father function
			exit
		fi
	done
	echo "0" #Since there's no 0 id, 0 means mac not found
	exit
}

id_mac()
{
#It's a wrapper. For each room there is a mac list to verify
	for mac in ${macs[@]}; do	#The computer may have more than 1 mac
		for dir in $(ls $WORKDIR); do	#List all rooms
			if [ -a $WORKDIR/$dir/data/macs ]; then	#Take the mac list of the room and search
				number="$(look_in $WORKDIR/$dir/data/macs)"
				if [ "$number" != "0" ]; then
					echo "$dir$number"	#If found, return the PCID
					exit
				fi
			fi
		done
	done
	echo "$DEFAULT_DIR""$( expr $(tail -n 1 $WORKDIR/$DEFAULT_DIR/data/macs | cut -b 1-2) + 01 )" #Update the machine ID based on unaddressed pcs
	exit
}

room_of()
{
	echo "$( echo $1 | cut -b 1-3 )"	#Extract the room number, given the PCID
}

id_of()
{
	echo "$( echo $1 | cut -b 4-5 )"	#Extract the machine id, given the PCID
}

room_exists()
{
#Verify the existence of a room
	if [ -d $WORKDIR/"$(room_of $1)" ]; then
		echo "0"
	else
		echo "1"
	fi
	exit
}

validate_pcid()
{
#Validate the PCID. It MUST have 5 numbers, and the room must exist. If not valid, return the default PCID
	validate=$1

	if [ ${#validate} -eq 5 ]; then
		if [ "$(room_exists $( room_of $validate ) )" = "0" ];then
			echo $validate
		else
			echo "$DEFAULT_DIR""$($id_of $validate)"
		fi
	else
		echo "$DEFAULT_DIR""$( expr $(tail -n 1 $WORKDIR/$DEFAULT_DIR/data/macs | cut -b 1-2) + 01 )" #Update the machine ID based on unaddressed pcs
	fi
}

add_mac()
{
#Save the machine MAC's in a specified file, by PCID
	if ! [ -d "$WORKDIR/$(room_of $1)" ] ; then
		PCID="$DEFAULT_DIR$( id_of $1 )"
	else
		PCID="$1"
	fi

	ROOM="$(room_of $PCID)"

	for mac in ${macs[@]}; do	#For every mac for this computer, we save each with one numeral
		echo "$( id_of $PCID )"'-'$mac >> $WORKDIR/$ROOM/data/macs
		PCID="$(validate_pcid "$(( 10#$(expr $PCID) + 1 ))")"
	done
}

change_id()
{
#It's a wrapper. After removing a specified mac from a list, adds to another one
	NEW_PCID="$(validate_pcid $1)"
	ROOM=$(room_of $PCID)
	NUMBER=$(id_of $PCID)
	if [ -a $WORKDIR/$ROOM/data/macs ]; then
		index=0
		for i in $(cat $WORKDIR/$ROOM/data/macs); do
			lista[index]=$i
			index=$(( $index + 1 ))
		done

		for mac in ${macs[@]}; do
			index=0
			while [ $index -lt ${#lista[@]} ]; do
				if [ "$mac" = "$( echo ${lista[$index]} | cut -b 4-20)" ]; then
					lista=( ${lista[@]:0:$index} ${lista[@]:$(( $index + 1 ))} )
					index=$(( $index - 1 ))
				fi
				index=$(( $index + 1 ))
			done
		done

		echo ${lista[0]} > $WORKDIR/$ROOM/data/macs
		for i in ${lista[@]:2}; do
			echo $i >> $WORKDIR/$ROOM/data/macs
		done

	fi
	add_mac $NEW_PCID
	echo $NEW_PCID
	exit
}

wait_input()
{
#Waits for input, or continues the procedure
	if [ $1 -ge 0 ]; then
		mp=$(( $TIME*$1 ))
	else
		mp=$TIME
	fi

	read -n 1 -t $mp
	if [ $? == 0 ]; then
		echo "1"
		exit
	fi
	echo "0"
	exit
}

verify_int()
{
	if ! [ -a tmp/last_good.tar ] && ! [ -a tmp/hash ]; then
		echo "1"
		exit
	fi

	tar cfv tmp/verify.tar spec > tmp/verify.log	#Generate a new tar to verify the directory
	compromissed=$( sha512sum --quiet -c tmp/hash )	#If sha512sum retuns something, it's the warning that the file is compromissed

	if [ -z "$compromissed" ]; then	#If the variable is set, the file is compromissed
		mv tmp/verify.tar tmp/last_good.tar	#If not compromissed, update the good tar and the good log
		mv tmp/verify.log tmp/last_good.log
		echo "0"
	else
		echo "1"
	fi
	exit
}

update_hash()
{
#Forces the hash to be updated, as well the backup file
	tar cfv tmp/last_good.tar spec/ > tmp/last_good.log	#Generate a new tar of the directory
	echo $(sha512sum tmp/last_good.tar) > tmp/hash	#Generate and save the new hash
}

uptodate()
{
#Update the computer, duh!
	rm -f /var/lib/dpkg/lock* #Removing apt lock, due to killing problems
	rm -f /var/lib/apt/lists/lock

	apt update
	apt -y full-upgrade
	apt -y autoremove
}


build_lists()
{
#Building apps installation and removal lists

	echo "Building apps lists"

	cat $WORKDIR/$DEFAULT_DIR/$INSTALL > /tmp/prog_tmp #Save the general apps list

	if [ "$ROOM" != "$DEFAULT_DIR"] && [ -a $WORKDIR/$ROOM/$INSTALL ]; then	#if there a specific room list, concatenate
		cat $WORKDIR/$ROOM/$INSTALL >> /tmp/prog_tmp
	fi
	cat $WORKDIR/$DEFAULT_DIR/$REMOVE > /$REMOVE #Read the to-remove default list

	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/$REMOVE ]; then #Read the specified room list as well
		cat $WORKDIR/$ROOM/$REMOVE >> /$REMOVE
	fi
	cat /tmp/prog_tmp | grep -v '$(cat /$REMOVE)' > /$INSTALL #Remove the to-remove apps from install list, due possible colision problems

	echo "Uninstalling unwanted packages"
	for i in $(cat /$REMOVE); do
		apt -y purge $i
	done

	#Installing listed programs in prog_ins

	echo "Installing wanted packages"
	for i in $(cat /$INSTALL); do
		apt -y install $i
	done

	rm /$INSTALL #clean up the mess
	rm /$REMOVE
	rm /tmp/prog_tmp
}

loopback_conf()
{
#Installation of loopback script for aluno incognito user
	echo "Time tavel machine time (loopback script)..."

	cp $WORKDIR/$DEFAULT_DIR/$SCRIPT /$SCRIPT

	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/$SCRIPT ]; then	#If there's a specific room config file, use the room file
		cat $WORKDIR/$ROOM/$SCRIPT >> /$SCRIPT
	fi

	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/$SERVICE ]; then	#Same idea to daemon file
		cp $WORKDIR/$ROOM/$SERVICE /$SERVICE
	else
		cp $WORKDIR/$DEFAULT_DIR/$SERVICE /$SERVICE
	fi

	chmod 450 /$SCRIPT	#Change files permission to avoid unwanted access
	chmod 440 /$SERVICE

	systemctl disable loopback.service	#Reset the Daemon for reseting user script
	systemctl enable loopback.service
}


hostname_config()
{
		if [ $( expr $PCID ) -lt 20000 ]; then	#If PCID it's not de default, defines the machine hostname as this.
			hostnamectl set-hostname $PCID
		fi
}

post_clear()
{
#Clear for-support installed apps, to avoid unecessary apps
	cat $WORKDIR/$DEFAULT_DIR/prog/prog_aft > /tmp/prog_aft
	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/prog/prog_aft ]; then
		cat $WORKDIR/$ROOM/prog/prog_aft >> /tmp/prog_aft
	fi

	for i in $(cat /tmp/prog_aft); do
		apt purge -y $i
	done

	apt -y autoremove
	apt -y clean
}

grubify()
{
#Update grub config files
	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/$GRUB ]; then
		cp $WORKDIR/$ROOM/$GRUB /$GRUB
	else
		cp $WORKDIR/$DEFAULT_DIR/$GRUB /$GRUB
	fi
	update-grub
}

package_config()
{
	#Instaling Atom, Arduino IDE, NetBeans and GNU-Octave

	echo "installing pre-compiled apps"
	if [ -a "$WORKDIR/$ROOM/scripts/prec_ins.sh" ]; then
		bash $WORKDIR/$ROOM/scripts/prec_ins.sh $WORKDIR
	else
		bash $WORKDIR/$DEFAULT_DIR/scripts/prec_ins.sh $WORKDIR
	fi

}

finish()
{
	#Removing installed suport programs, they're not needed anymore, listed in prog_aft

	echo "Removing apps installed due script operations"
	post_clear

	echo "After job's for this room, if any (specifc room scripts)"
	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/scripts/after.sh ]; then
		bash $WORKDIR/$ROOM/scripts/after.sh
	fi
	uptodate
}

find_impostors()
{
#Serach for non-default users on the computer, and delete them
	users=$(compgen -u) #List the users names
	for user in $users; do
		useruid=$(id -u $user) #Get the uid of each user
		if [ $useruid -gt 999 ] && [ $useruid -lt 65534 ]; then #it means i'ts not a system/default user
			delete=$(cat $WORKDIR/$ROOM/default_users | grep $user) #Compare with the default user list
			if [ -z $delete ]; then
				deluser --remove-all-files $user
			fi
			unset delete
		fi
	done
}

user_config()
{
#Add the admin user and remove default unwanted users permissions
	echo "You may pass (adding users)"

      find_impostors

	useradd -m -d /home/$LUSER -s /bin/bash $LUSER
	useradd -m -r -s /bin/bash $AUSER

	echo "Just to be sure, i'll reset users passwords for you :)"

	#echo "$(cat /$SHADOW | grep -v $AUSER | grep -v $LUSER)" >> /tmp/shadow

	if [ -z "$AUSER_P" ]; then
		passwd -d $AUSER
	else
		echo "$AUSER:$AUSER_P:0:0:99999:7:::" >> /tmp/shadow
	fi

	if [ -z "$LUSER_P" ]; then
		passwd -d $LUSER
	else
		echo "$LUSER:$LUSER_P:0:0:99999:7:::" >> /tmp/shadow
	fi

	mv /tmp/shadow /$SHADOW
	chmod 640 /$SHADOW

	cat /$SUDOERS | grep -v %sudo | grep -v %admin > /tmp/sudoers
	cat $WORKDIR/$DEFAULT_DIR/$SUDOERS >> /tmp/sudoers

	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/$SUDOERS ]; then
		cat $WORKDIR/$ROOM/$SUDOERS >> /tmp/sudoers
	fi

	mv /tmp/sudoers /$SUDOERS

	echo "And you may not... (removing unecessary permissions and groups)"
	groupdel -f sudo
	groupdel -f admin
}

goodbye()
{
#GOOD-FCKN-BYE
	echo "Shuting down... press any buton to menu"
	if [ "$(wait_input 100)" = "1" ]; then
		help
		read op
		menu ${op[@]}
		exit
	fi

	echo "Goodbye :)"
	shutdown -Ph now
}

hello()
{
	echo "#######################################################"
	echo "#   		  Hello, script!		    #"
	echo "#   Computer configuration script, by Allan Ribeiro   #"
	echo "#     email to:<allanribeiro@alunos.utfpr.edu.br>     #"
	echo "#######################################################"
}

help()
{
	echo "#######################################################"
	echo "#	 current PCID :$PCID - To change, use function 3    #"
	echo "#		 Choose a number for a function	    #"
	echo "# 1) Verify program	  2) Update hashes	    #"
	echo "# 3) Choose PCID	  4) Change/add MAC	    #"
	echo "# 5) Config. users	  6) Update		    #"
	echo "#	7) Default programs (apt)  8) Default programs(prec)#"
	echo "#	9) Script Loopback	  10) Config. Hostname	    #"
	echo "#	11) grubify		  12) Script After-Jobs	    #"
	echo "#	13) Say Hello, script!	  14) Exit		    #"
	echo "#######################################################"
}

menu()
{
	source $WORKDIR/$DEFAULT_DIR/variables
	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/variables ]; then
		source $WORKDIR/$ROOM/variables
	fi

	for parameter in $@; do
		case $parameter in
			1 )
				if [ $(verify_int) -eq 0 ]; then
					echo "A file has been modified. Verify \"compromissed\" for more details."
				else
					echo "No alterations has been found."
				fi
				;;
			2 )
				update_hash
				;;
			3 )
				echo "Type the PCID in xyzab format (ex. 00120 for room 001, machine 20)"
				read NEW_PCID
				if ! [ -d $WORKDIR/"$(room_of $PCID)" ]; then
					NEW_PCID="$DEFAULT_DIR""$(id_of $PCID)"
				fi
				ROOM=$(room_of $NEW_PCID)
				source $WORKDIR/$ROOM/variables
				;;
			4 )

				change_id
				;;
			5 )
				user_config
				;;
			6 )
				uptodate
				;;
			7 )
				build_lists
				;;
			8 )
				package_config
				;;
			9 )
				loopback_config
				;;
			10 )
				hostname_config
				;;
			11 )
				grubify
				;;
			12)
				finish
				;;
			13 )
				hello
				;;
			14 )
				exit
				;;
			* )
				echo "Invalid parameter"
				help
				exit
		esac
	done
}

default_procedure()
{

	source $WORKDIR/$DEFAULT_DIR/variables
	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/variables ]; then
		source $WORKDIR/$ROOM/variables
	fi

	#Users setting

	echo "Configuring users"
	user_config

	#Updating and installing needed packages

	echo "Updating system"
	uptodate

	#Building apps installation and removal lists

	echo "Building apps list"
	build_lists

	#Removing listed programs in prog_rem

	echo "Now, configuring apps"
	package_config

	#Installation of loopback script for aluno incognito user

	echo "Time travel machine time (loopback script)..."
	loopback_conf

	#Hostname configuration

	echo -e "Changint the machine\'s hostname"
	hostname_config

	#GRUB reconfiguration

	echo "Reconfiguring grub"
	grubify

	#Finishing

	echo "last package verification before remove support-to-script apps"
	finish

	goodbye
}


main()
{
	if [ "$(whoami)" = "root" ]; then #Check if the user has the permission to run this script

		echo "Verifying program's integrity"
		if [ $(verify_int) != 0 ]; then
			echo "A file has been modified. Exiting"
			exit
		fi

		WORKDIR="$(pwd)/spec"	#Path to room's folder
		DEFAULT_DIR="200"	#If a room wasn't selected, the default room is used
		TIME=5

		hello	#Say hello, Script
		nmcli n connectivity check	#If not logged in, force the login screen to appear
		sleep $TIME

		macs=($(lshw | grep -A 13 network | grep serial | cut -b 25-41))	#Get the computer mac's
		PCID="$(id_mac)"	#Try to identify the mac
		ROOM="$(room_of $PCID)"	#Save the room number for the functions

		if ! [ $# -gt 0 ]; then #Check for parameters (specifc script functions). If no parameters has been given, run program normally
			echo "PCID is $PCID. Is this right? Press any key, if not"	#In case of the user want to change the mac identity
			if [ "$(wait_input 1)" = "1" ]; then
				echo "The format of PCID is ROOMMACHINE, as 00902 for room 009 and machine 02"
				echo "If the id is invalid, the mac saved in $DEFAULT_DIR room, and the default instalation will follow"
				echo "Type the PCID"
				read NEW_PCID
				PCID=$(change_id $NEW_PCID)
				ROOM="$(room_of $PCID)"	#Save the room number for the functions
			fi

			default_procedure

		else #Call specifc script funtions
			menu $@
		fi
	else
		echo "Thanks for avoiding root. But this script needs it (try with sudo)"
	fi
}

main $@

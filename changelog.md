# Changelog (release 1)

* Unidentified computers now have a variable of PCID, according to the number of unindentified computers

* Print bug in validate_hash fixed

* Added a funtion in loopback script for deleting non-default users

* Pt version fixed

* Update-manager package put in prog_rem (prevents FU***** ANOYING UPDATE POPUP from appearing)
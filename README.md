# sysconfig.sh - DAINF, UTFPR

## NOME

sysconfig.sh - Faz a configuração do sistema operacional de acordo com o padrão do bloco V

## SINOPSE

sysconfig.sh [_opções_]

## DESCRIÇÃO

Sua premissa é facilitar o gerenciamento das máquinas do bloco V. Temos diversos computadores, o que torna inviável a manutenção física de todos de fo
rma periódica. Esse script facilita a configuração, necessitando apenas de uma supervisão. Ainda será escrito um script para verificar o sucesso deste
 script.

## OPÇÕES

OBS.: Todas as opções marcadas por * são **personalizadas**

1 - Faz a verificação das hashs do programa

2 - Refaz todas as hashes, para efetivar alterações em arquivos

3 - Altera o PCID detectado

4 - **change_id()** - Faz reatribuição da identidade do MAC

5* - **user_config()** - Reconfigura usuários e permissões padrão para o script de loopback

6 - **uptodate()** - Atualiza o computador

7* - **build_lists()** - Faz a instalação e remoção do programas padrões

8* - **package_config()** - Faz a instalação de softwares pré-compilados

9* - **loopback_config()** - Faz a instalação do script de loopback

10* - **grubify()** - Faz a sobrescrita do arquivo de configuração do grub

11* - **finish()** - Faz remoção dos apps de suporte, limpeza de lixo e execução de **after** scripts.

12 - **hello()** - Apresenta a tela de cumprimentos ao usuário.

13 - Sai

## FUNÇÕES

As funções serão explicadas na ordem em que são invocadas na operação padrão de instalação, **default_procedure**, que ocorre sem intervenção do usuário.

### main

Consiste na função pai do processo. Ela é responsável por encaminhar o menu de escolhas, e dar início aos procedimentos escolhidos.

A função verifica se o usuário tem permissões administrativas, garantindo que todos os arquivos de configurações e programas sejam instalados 
sem interação por entrada padrão.

A intergridade do programa é verificada através da **verify_int**. Caso nenhum problema seja encontrado, continua normalmente. Caso algum hash não
se confirme, o programa sai.

O programa tem como varíável mestre **$WORKDIR**, que contém o caminho de onde o programa é rodado. Não existem problemas em rodar de um diretó
rio qualquer, desde que a estrutura padrão do programa seja mantido. Quaisquer alterações adicionais ficam a cargo do administrador do siste
ma.

O contexto de apresentação é mostrado, e segue-se um teste de conectividade. Como a UTFPR exige login via rede, este comando deve forçar a pá
gina de login ser aberta. Segue-se um período de espera para que o login seja feito.

Os macs estão localizados em **$WORKDIR/[sala]/data/macs**, sendo 200 uma sala coringa, para computadores não localizados em uma sala específica.

O formato padrão da **$PCID** é salamaquina, como por exemplo, 00120 para máquina 20 da sala V001
Caso não encontrado, o formato padrão para que seja adicionado é 200XY, sendo XY um número entre 01 e 99

Verifica ainda, se foi chamada com algum argumento, para personalizar o procedimento, ou foi chamada normalmente, culminando na execução da fu
nção **default_procedure()**

#### Sem argumentos - procedimento padrão 

O administrador pode, ou não, alterar a **$PCID** conforme vontade, presionando qualquer tecla num período especificado por **$TIME**. Caso nenhum
a interação seja feita, o processo segue com o **$PCID** mostrado.
Se o PCID especificado for considerado inválido, como por exemplo, 210, derivado de uma sala que não existe, ele tomará formato padrão de novo
s computadores, iniciando com 200.

alterar a **$PCID** de um computador implica em remover seus macs da lista original, e gerar uma quantidade de novas linhas, equivalente ao núme
ro de dispositivos com mac, na lista da sala especificada. O arquivo pode ou não estar ordenado.

#### Com arguementos - Passos específicos

A função **menu()** é chamada com os argumentos dados, executando eles na ordem inserida.
Caso algum argumento seja inválido, o programa apresenta a lista de opções e sai.

### verify_int

Não recebe argumentos. A função cria um tar do diretório **/spec** em **tmp/verify.tar**, para gerar sua hash, levando assim em conta arquivos criados, apagados, ou
modificados. Verifica então a hash do novo arquivo criado, comparando-o com o último tar saudável gerado, e se forem iguais, sobrescreve, o antigo
tar saudável, garantindo assim que o arquivo esteja sempre atualizado.
Caso sejam diferentes, salva o tar gerado para verifição pelo adm, salvando também o log do tar gerado.

### update_hash

Força a atualização da hash e dos arquivos de segurança, gerando um novo tar, sobrescrevendo a antiga hash e o antigo log.

### id_mac

Não recebe argumentos. Faz um chamado de **look_in** para todas os diretórios listados em **spec/**, por endereço mac da máquina. Aloca o 
retorno de **look_in** e avalia: se for diferente de zero, retorna o número da sala no qual encontrou, concatenado ao resultado de **look
_in**, o que configura o padrão de **PCID**. Caso contrário, retorna **$DEFAULT_DIR01**.

### look_in

Recebe o caminho de um arquivo de texto, contendo a lista de macs de uma sala, como argumento. Extrai o endereço mac e compara, na ord
em do arquivo, os macs da máquina. Se o mac da máquina estiver contido no arquivo, retorna o número da máquina. Caso contrário, retorn
a 0.

### room_of

Faz a extração do número da sala, a partir do **PCID**.

### id_of

Faz a extração do número de identidade da máquina, a partir do **PCID**

### change_id

Recebe como argumento um padrão **PCID**. Verifica se a máquina contém o mac na lista identifiada **PCID** (not the argument) retornado por **id_mac**, e caso 
se confirme, remove todos os macs correspondentes à esta máquina do arquivo.
Por último, repassa o argumento padrão **PCID** recebido para a função **add_mac**

### add_mac

Pode receber como argumento um padrão **PCID**. Se receber, valida o padrão através de **validate_pcid**. Independente da validade, adiciona todos os 
macs presentes em **macs** ao arquivo de macs da sala especificada.

### validate_pcid

Recebe um padrão **$PCID** como argumento. Para validar, verifica primeiro o tamanho da string (deve ter 5 caracteres, 3 para a sala, e 2 para o id). 
Caso sim, verifica se a sala existe. Se sim, retorna o PCID, caso não, retorna **$DEFAULT_DIR** seguido do id contido no PCID não válido, se invalida-
do pela não existência da sala, ou 01, se invalidado por tamanho 

### default_procedure

Esta é a função mestre que realiza o procedimento padrão da configuração. Não recebe qualquer parâmetro. É, na verdade, um wrapper.

#### nota do programador:

	**_personalizar_** um arquivo diz respeito à escolher este arquivo entre o de uma sala específica, se presente, e o arquivo
	padrão. O arquivo específico tem prioridade sobre o geral. Dado que um arquivo /etc/example esteja para ser personalizado, 
	para a sala 002, o arquivo desta máquina será substituido pelo arquivo da sala, com as devidas adaptações. Caso o arquivo 
	não seja encontrado, **$DEFAULT_DIR** DEVE conter um arquivo para substituir o ausente, o que configura dizer que sejá usa
	do uma configuração padrão.

### user_config

Ela altera as permissões do arquivo à personalizar para o padrão (640), e personaliza o arquivo /etc/shadow.

### uptodate

Faz a remoção dos arquivos de trava do apt, para forçar a atualização a acontecer. Existe o risco de quebrar pacotes caso outra instância do apt
 rode ao mesmo tempo.
Por ultimo faz a atualização do computador, conforme apt full-upgrade.

### build_lists

Gera a lista de aplicativos a serem instalados e removidos do computador, através da concatenação dos arquivos **$WORKDIR/$ROOM/prog/prog_ins** 
específico de uma sala e o contido em **$DEFAULT_DIR**. O mesmo vale para **prog_rem**. A lista de remoção tem prioridade sobre a lista de instalação
, ou seja, se um programa exemplo1 está, ao mesmo tempo na lista de instalação e de remoção, ele não será instalado.

Em seguida, ele remove os aplicativos da lista gerada em **/tmp/prog_rem** usando **apt -y purge**
E instala os aplicativos da lista gerada em **/tmp/prog_ins** usando **apt -y install**

Por último, remove as listas geradas.

### package_config

Esta função é um wrapper. Faz a chamada de outro script **personalizado**, **prec_ins.sh**, responsável por instalar programas pré-compilados de forma
 personalizada, conforme cada programa exige.

### loopback_conf

Faz a ativação do script **personalizado** loopback_conf, localizado em $SCRIPT. Este script é instalado como um serviço, também **personalizado**, i
ndicado por **$SERVICE**. Depois redefine as permissões de usuário como 450 e 440, respectivamente.

Por último, faz a habilitação do serviço através do systemctl  

### grubfy

Faz a **personalização** do script para gerar o arquivo de configuração do grub, e confirmando depois, quando gera um novo arquivo de configuração, us
ando **update-grub**

### finish

Faz o chamado da função **post_clear**, seguido do chamado de outro script, o **personalizado after.sh**

### post_clear

Alguns aplicativos são instalados apenas para dar suporte à instalação de outros apps. Esta função faz a remoção destes apps, **personalizados** em **
prog_aft**, usando **apt -y purge**

# BUGS

Este script não deve ser rodado concomitantemente ao apt (salvo a execução do apt pelo próprio script). Veja mais em **uptodate**

![](https://gitlab.com/kresserbash/linux-adm/-/wikis/img/logo_UTFPR.gif "Logo UTFPR")

Universidade Tecnológica Federal do Paraná, Pato Branco
